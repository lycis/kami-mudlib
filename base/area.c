#include "area.h"
#include "commands.h"
#include "ansi.h"
#include <xml.h>



inherit "/base/coordinates";
inherit "/base/zone";

nosave mixed *_map = ({ });
nosave mapping _zones = ([ ]);
nosave int _width, _height;

public void create() {
    if(clonep(this_object())) {
        raise_error("illegal attempt to clone area");
        destruct(this_object());
    }

    zone::create();

    _zones += ([ Z_GLOBAL: this_object() ]);

    add_zone_action("move_north", ({"n", "north"}));
    add_zone_action("move_east", ({"e", "east"}));
    add_zone_action("move_south", ({"s", "south"}));
    add_zone_action("move_west", ({"w", "west"}));
}

nomask int is_area() {
    return 1;
}

nomask public void initialize_map(int width, int height) {
    if(sizeof(_map) > 0) {
        raise_error("forbidden resizing of area");
        return;
    }

    _map = ({ });
    for(int y = 0; y < height; y++) {
        mixed *line = ({ });
        for(int x = 0; x < width; x++) {
            line += ({ 0 });
        }
        _map += ({ line });
    }

    _width = width;
    _height = height;
}

public struct Locale locale_at(int x, int y) {
    struct Locale l = _map[x][y];
    if(l==0) {
        struct Locale l1 = (<Locale> ({Z_GLOBAL}), ([]));
        _map[x][y] = l1;
        return l1;
    }
    return l;
}

public int can_move_to(object ob, struct Coordinates from, struct Coordinates to) {
    if(to->y >= sizeof(_map) || to->y < 0 ||
       to->x >= sizeof(_map[0]) || to->x < 0) {
        tell_object(ob, "You cannot go beyond the map.\n");
        return 0;
    }

    struct Locale fromLocale = locale_at(from->x, from->y);
    string barrierId = sprintf("%d:%d:%d", to->x, to->y, to->z);
    if(member(fromLocale->barriers, barrierId)) {
        string *barrier = fromLocale->barriers[barrierId];
        tell_object(ob, sprintf("%s %s.", capitalize(barrier[0]), barrier[1]));
        return 0;
    }

    return 1;
}

private int move_player(int stepX, int stepY) {
    struct Coordinates oldPosition = this_player()->get_current_position();
    struct Coordinates newPosition = (<Coordinates> 
        oldPosition->area, 
        oldPosition->x + stepX,
        oldPosition->y + stepY);

    if(move_to_coordinates(this_player(), newPosition) != 1) {
        return 1;
    }

    return 1;
}

public int move_north(string args) {
    if(query_command() != "n" && query_command() != "north") {
        return 0;
    }
    return move_player(0, -1);
}

public int move_east(string args) {
    if(query_command() != "e" && query_command() != "east") {
        return 0;
    }
    return move_player(1, 0);
}

public int move_south(string args) {
    if(query_command() != "s" && query_command() != "south") {
        return 0;
    }
    return move_player(0, 1);
}

public int move_west(string args) {
    if(query_command() != "w" && query_command() != "west") {
        return 0;
    }
    return move_player(-1, 0);
}

public void on_enter(object ob) {
}

public object get_zone_object(string zone) {
    mixed entry = _zones[zone];
    if(objectp(entry)) {
        return entry;
    }

    if(stringp(entry)) {
        return find_or_load_object(entry);
    }

    raise_error(sprintf("illegal zone reference entry (%s -> %O)", zone, entry));
}


public void on_moved_to(object ob, struct Coordinates newPosition) {
    return;
}

public void on_moved_from(object ob, struct Coordinates oldPosition) {
    return;
}

public void add_zone(int **coordinates, string id, mixed reference) {
    _zones += ([id: reference]);
    foreach(int *point: coordinates) {
        struct Locale locale = locale_at(point[0], point[1]);
        if(member(locale->zones, id) < 0) {
            locale->zones += ({id});
        }
    }
}

public int execute_area_command(string input) {
    if(environment(this_player()) != this_object()) {
        return 0;
    }

    struct Coordinates currentPosition = this_player()->get_current_position();
    if(!currentPosition) {
        return 0;
    }

    struct Locale loc = locale_at(currentPosition->x, currentPosition->y);
    foreach(string zone: loc->zones) {
        object zobj = get_zone_object(zone);
        if(!objectp(zobj)) {
            continue;
        }

        if(zobj->execute_zone_command(input)) {
            return 1;
        }
    }

    return 0;
}

private string get_zone_description_at(int x, int y) {
    string description = "";
    foreach(string zone: locale_at(x, y)->zones) {
        object zobj = get_zone_object(zone);
        description += zobj->get_description() + " ";
    }
    return description;
}

private int** get_coorindates_in_sight(object living) {
    int **coordinates = ({});
    int r = living->get_vision_range();
    int x0 = living->get_current_position()->x;
    int y0 = living->get_current_position()->y;

    if(r < 1) r = 0;

    for(int x = -r; x <= r; ++x) {
        for(int y = -r; y <= r; ++y) {
            if(x*x + y*y <= r*r) {
                int modX = x0+x;
                int modY = y0+y; 

                if(modX > 0 && modX < sizeof(_map[0]) && modY > 0 && modY < sizeof(_map)) {
                    coordinates += ({ ({modX, modY, 0}) });
                }
            }
        }
    }

    return coordinates;
}

#define CD_NORTH       1
#define CD_NORTHEAST   2
#define CD_EAST        4
#define CD_SOUTHEAST   8
#define CD_SOUTH      16
#define CD_SOUTHWEST  32
#define CD_WEST       64
#define CD_NORTHWEST 128
#define CD_SAME      256

#define PI 3.14159265359

private int get_cardinal_direction(int *from, int *to) {
    int relX = to[0] - from[0];
    int relY = to[1] - from[1];

    if(relX == 0 && relY == 0) return CD_SAME;

    float phi = 0;
    if(relX != 0) {
        phi = atan2(relY, relX); // * 57,2958;
        phi = (phi > 0 ? phi : (2*PI + phi)) * 360 / (2*PI);
    } else {
        if(relY > 0) phi = 90.0;
        else         phi = 270.0;
    }

    if(phi < 12 || phi >= 348)        return CD_EAST;
    else if(phi >= 12 && phi < 78)    return CD_SOUTHEAST;
    else if(phi >= 78 && phi < 102)   return CD_SOUTH;
    else if(phi >= 102 && phi < 168)  return CD_SOUTHWEST;
    else if(phi >= 168 && phi < 192)  return CD_WEST;
    else if(phi >= 192 && phi < 258)  return CD_NORTHWEST;
    else if(phi >= 258 && phi < 282)  return CD_NORTH;
    else if(phi >= 282 && phi < 348)  return CD_NORTHEAST;

    return -1;
}

private string direction_to_str(int direction) {
    string *descr = ({});

    if(direction & CD_NORTH) descr += ({ "north" });
    if(direction & CD_NORTHEAST) descr += ({ "northeast" });
    if(direction & CD_EAST) descr += ({ "east" });
    if(direction & CD_SOUTHEAST) descr += ({ "southeast" });
    if(direction & CD_SOUTH) descr += ({ "south" });
    if(direction & CD_SOUTHWEST) descr += ({ "southwest" });
    if(direction & CD_WEST) descr += ({ "west" });
    if(direction & CD_NORTHWEST) descr += ({ "northwest" });

    string sentence = "";
    for(int i = 0; i < sizeof(descr); ++i) {
        string dir = descr[i];
        if(i == sizeof(descr)-2) {
            sentence += dir + " and ";
        } else {
            sentence += dir + ", ";
        }
    }

    return sentence[0..sizeof(sentence)-3];
}

public string* current_zones_of(object who) {
    if(environment(who) != this_object()) {
        return ({ });
    }

    int x = who->get_current_position()->x;
    int y = who->get_current_position()->y;

    return locale_at(x, y)->zones;
}

private string get_remote_descriptions_in_sight(object who) {
    int **visibleCoordinates = get_coorindates_in_sight(who);
    
    int currentX = who->get_current_position()->x;
    int currentY = who->get_current_position()->y;
    
    string *currentZones = locale_at(currentX, currentY)->zones;
    mapping zones = ([]);

    foreach(int *coordinate: visibleCoordinates) {
        string *potential_new_zones = locale_at(coordinate[0], coordinate[1])->zones;
        foreach(string z: potential_new_zones) {
            int cardinalDirection = get_cardinal_direction(({currentX, currentY}), ({coordinate[0], coordinate[1]}));
            if(member(zones, z)) {
                zones[z] |= cardinalDirection;
            } else if(member(currentZones, z) < 0) {
                zones += ([z: cardinalDirection ]);
            }
        }
    }

    string description = "";
    foreach(string zone: zones) {
        description += terminal_colour(sprintf("You see %^BOLD%^%^WHITE%^%s%^NORMAL%^ in the %^BOLD%^%^WHITE%^%s%^NORMAL%^.\n", 
                            get_zone_object(zone)->get_remote_description(), direction_to_str(zones[zone])),
                            DEFAULT_ANSI_COLOURS);
    }
    return description;
}

private string get_barrier_description_at(int x, int y) {
    struct Locale l = locale_at(x, y);
    string descr = "";
    foreach(string key, string* value: l->barriers) {
        int tX, tY, tZ;
        sscanf(key, "%d:%d:%d", tX, tY, tZ);
        int cd = get_cardinal_direction(({x, y}), ({tX, tY}));
        descr += sprintf(value[2], direction_to_str(cd)) + "\n";
    }
    return descr;
}

public string get_description() {
    if(environment(this_player()) != this_object()) {
        return 0;   
    }

    if(previous_object() == this_object()) {
        return zone::get_description();
    }

    struct Coordinates currentPosition = this_player()->get_current_position();
    string general_description = get_zone_description_at(currentPosition->x, currentPosition->y);
    string remote_descriptions = get_remote_descriptions_in_sight(this_player());
    string barrier_description = get_barrier_description_at(currentPosition->x, currentPosition->y);

    string description = sprintf("  %s\n", general_description);
    if(sizeof(remote_descriptions) > 0) {
        description += sprintf("  %s", remote_descriptions);
    }
    if(sizeof(barrier_description) > 0) {
        description += sprintf("  %s\n", barrier_description);
    }
    return description;
}

public void add_barrier(int *coordinateA, int *coordinateB, string name, string block_string, string description) {
    struct Locale a = locale_at(coordinateA[0], coordinateA[1]);
    struct Locale b = locale_at(coordinateB[0], coordinateB[1]);

    a->barriers[sprintf("%d:%d:%d", coordinateB[0], coordinateB[1], coordinateB[2])] = ({ name, block_string, description });
    b->barriers[sprintf("%d:%d:%d", coordinateA[0], coordinateA[1], coordinateA[2])] = ({ name, block_string, description });
}

#ifdef __XML_DOM__
struct tmxTile {
    string zoneFile;
    string zoneId;
    int barrierDirection;
};

private void tmx_check_top_level_tag_or_die(mixed *xml) {
    string topLevelTag = xml[XML_TAG_NAME];
    if(topLevelTag != "map") {
        raise_error("invalid tmx format");
    }
}

private void tmx_initialize_map(mixed *xml) {
    mapping attributes = xml[XML_TAG_ATTRIBUTES];
    int width = to_int(attributes["width"]);
    int height = to_int(attributes["height"]);
    initialize_map(width, height);
}

private void tmx_apply_tile(int x, int y, struct tmxTile tile) {
    if(tile->zoneId != 0 && tile->zoneFile != 0) {
        add_zone( ({ ({ x, y, 0}) }), tile->zoneId, tile->zoneFile);
    }
}


private void tmx_apply_layer(mixed *layer, mapping tiles) {
    string *csv = ({ });
    foreach(mixed *e: layer[XML_TAG_CONTENTS]) {
        if(e[XML_TAG_NAME] == "data") {
            csv = explode(e[XML_TAG_CONTENTS][0], ",");
            break;
        }
    }

   for(int i=0; i<sizeof(csv); i++) {
       int x = i % _width;
       int y = i / _width;
       int tileId = to_int(csv[i]);
       if(member(tiles, tileId)) {
           tmx_apply_tile(x, y, tiles[tileId]);
       }
   }
}

private void tmx_apply_all_layers(mixed *xml, mapping tiles) {
    foreach(mixed *element: xml[XML_TAG_CONTENTS]) {
        if(element[XML_TAG_NAME] == "layer") {
            tmx_apply_layer(element, tiles);
        }
    }
}

private struct tmxTile create_tile_from_tmx_element(mixed *element) {
    struct tmxTile tile = (<tmxTile>);

    foreach(mixed *subelement: element[XML_TAG_CONTENTS]) {
        if(subelement[XML_TAG_NAME] == "properties") {
            foreach(mixed *property: subelement[XML_TAG_CONTENTS]) {
		write(sprintf("%O\n", property));
                switch(property[XML_TAG_ATTRIBUTES]["name"]) {
                    case "zoneFile": tile->zoneFile = property[XML_TAG_ATTRIBUTES]["value"]; break;
                    case "zoneId": tile->zoneId = property[XML_TAG_ATTRIBUTES]["value"]; break;
                }
            }
        }
    }
    
    return tile;
}

private mapping tmx_load_tileset(string src, int firstId, string basePath) {
    string tilesetFile = sprintf("%s%s", basePath, src);
    mixed *xml = xml_parse(read_file(tilesetFile));
    if(!xml) {
        raise_error("tileset not found");
    }

    mapping tiles = ([ ]);
    foreach(mixed *element: xml[XML_TAG_CONTENTS]) {
        if(element[XML_TAG_NAME] == "tile") {
            int id = firstId+to_int(element[XML_TAG_ATTRIBUTES]["id"]);
            struct tmxTile tile = create_tile_from_tmx_element(element);
            tiles[id] = tile;
        }
    }
    return tiles;
}

private mapping tmx_load_tilesets(mixed *xml, string basePath) {
    mapping tiles = ([ ]);
    foreach(mixed *element: xml[XML_TAG_CONTENTS]) {
        if(element[XML_TAG_NAME] == "tileset") {
            string source = element[XML_TAG_ATTRIBUTES]["source"];
            int firstId = to_int(element[XML_TAG_ATTRIBUTES]["firstgid"]);
            tiles += tmx_load_tileset(source, firstId, basePath);
        }
    }    
    return tiles;
}

nomask public void load_from_tmx(string filename) {
    string file_content = read_file(filename, 0, 0, "UTF-8");
    mixed* xml = xml_parse(file_content);
    
    int slashPos = strrstr(filename, "/");
    string filePath = filename[0..slashPos];

    tmx_check_top_level_tag_or_die(xml);

    tmx_initialize_map(xml);
    mapping tiles = tmx_load_tilesets(xml, filePath);
    tmx_apply_all_layers(xml, tiles);
}
#endif
