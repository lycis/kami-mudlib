struct Coordinates {
    string area;
    int x;
    int y;
    int z;
};

struct Locale {
    string *zones;
    // ([ "x:y:z":({string name, string block_string, string description}) ]) -> the barrier is from this locale to coordinate x:y:z
    mapping barriers;
};