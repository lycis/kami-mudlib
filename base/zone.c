inherit "/base/describable";

nosave mapping _zoneActions;

public void create() {
    describable::create();
    _zoneActions = ([ ]);
}

nomask public void add_zone_action(string func, string *verbs) {
    foreach(string verb: verbs) {
        m_add(_zoneActions, verb, func);
    }
}

nomask public mapping query_zone_actions() {
    return _zoneActions;
}

nomask public void apply_zone_actions() {
    mapping zactions = query_zone_actions();
    if(sizeof(zactions) > 0) {
        foreach(string command, string func: zactions) {
            add_action(func, command);
        }
    }
}

nomask public void remove_zone_actions() {
    mapping zactions = query_zone_actions();
    if(sizeof(zactions) > 0) {
        foreach(string command, string func: zactions) {
            remove_action(command);
        }
    }
}

public int execute_zone_command(string input) {
    string verb, args;
    int n = sscanf(input, "%s %s", verb, args);
    if(n != 2) {
        verb = input;
        args = "";
    }

    if(!member(_zoneActions, verb)) return 0;

    closure fun = symbol_function(_zoneActions[verb], this_object());
    if(!fun) {
        raise_error(sprintf("undefined function '%s' for zone '%O'", _zoneActions[verb], this_object()));
        return 0;
    }

    return funcall(fun, args);
}

