nosave mapping _details;
nosave string _remoteDescription;
nosave string _description;

public void create() {
    _description = "";
    _remoteDescription = "";
    _details = ([ ]);
}

public string get_description() {
    return _description;
}

public void set_description(string desc) {
    _description = desc;
}

public string get_remote_description() {
    return _remoteDescription;
}

public void set_remote_description(string desc) {
    _remoteDescription = desc;
}

public void add_detail(mixed keys, string description) {
    if(pointerp(keys)) {
        foreach(string key: keys) {
            add_detail(key, description);
        }
    } else {
        _details[keys] = description;
    }
}

public int has_detail(string key) {
    return member(_details, key);
}

public string get_detail(string key) {
    if(!has_detail(key)) {
        return 0;
    }

    return _details[key];
}
