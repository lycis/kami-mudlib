#include "/include/area.h"
inherit "/base/coordinates";

struct Coordinates _currentPosition;

public void create() {
    _currentPosition = (<Coordinates> "", 0, 0, 0);
}

public void set_current_positon(struct Coordinates position) {
    _currentPosition = position;
}

public struct Coordinates get_current_position() {
    return _currentPosition;
}
