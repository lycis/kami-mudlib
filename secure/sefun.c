inherit "/secure/sefun/move_to_coordinates";

public void create() {
    if(clonep(this_object())) {
        destruct(this_object());
    }
}

public object find_or_load_object(string name) {
    object ob = find_object(name);
    if(!objectp(ob)) {
        ob = load_object(name);
    }
    return ob;
}