inherit "/base/coordinates";

int move_to_coordinates(object ob, struct Coordinates newPosition) {
    object area = find_object(newPosition->area);

    if(!objectp(area)) {
        area = load_object(newPosition->area);
        if(!objectp(area)) {
            write("no area");
            return -1;
        }
    }

    if(!area->can_move_to(ob, ob->get_current_position(), newPosition)) {
//        write("can not move to\n");
        return 0;
    }

    struct Coordinates oldPosition = ob->get_current_position();

    if(load_name(environment(ob)) != newPosition->area) {
        move_object(ob, area);
    }

    ob->set_current_positon(newPosition);

    object oldArea = find_object(oldPosition->area);
    if(objectp(oldArea)) {
        oldArea->on_moved_from(ob, oldPosition);
    }
    area->on_moved_to(ob, newPosition);

    ob->on_moved(oldPosition, newPosition);
    
    return 1;
}