#include <files.h>
#include <input_to.h>
#include <ansi.h>

#define SAVEFILE_FORMAT_VERSION 2

// this is all the account data that we store. it is available in the login process.
string _username = "";
string* _characters = ({});

// prototypes
private object loadCharacter(string name);

static int logon() {
    if(file_size("/etc/WELCOME") > 0) {
        write(read_file("/etc/WELCOME"));
    }

    input_to("usernameEntered", INPUT_NOECHO|INPUT_PROMPT, 
             terminal_colour("%^YELLOW%^%^BOLD%^Your name:%^NORMAL%^ ", DEFAULT_ANSI_COLOURS));
    return 1;
}

string buildAccountSavefileName() {
    return sprintf("/accounts/%s", _username);
}

void startCharacterGeneration() {
    input_to("generateCharacter", INPUT_PROMPT, "What's the name of your character? ");
}

void displayCharacterSelection() {
    string* files = get_dir(sprintf("/characters/%s/", _username), GETDIR_NAMES);
    string* characters = ({});
    int nr = 1;

    foreach(string file: files) {
        string* parts = explode(file, ".");
        if(parts[1] != "o") continue;

        write(sprintf("%d. %s\n", nr++, capitalize(parts[0])));
        characters += ({parts[0]});
    }
    write(sprintf("%d. <new character>\n", nr));

    input_to("selectCharacter", INPUT_PROMPT, sprintf("Selected character [1-%d]: ", nr));
}

void selectCharacter(string selectedNumberStr) {
    int n;
    string err;

    if(err = catch(n=to_int(selectedNumberStr))) {
        write("Invalid selection. Only numbers are allowed.\n");
        displayCharacterSelection();
        return;
    }

    if(n > sizeof(_characters)+1 || n < 1) {
        write(sprintf("Invalid selection. Please select 1-%d.\n", sizeof(_characters)+1));
        displayCharacterSelection();
        return;
    } else if(n == sizeof(_characters)+1) {
        startCharacterGeneration();
    } else {
        loadCharacter(_characters[n-1]);
    }
}

private object loadCharacter(string name) {
    object playerObject;
    string err;

    if(err = catch(playerObject = clone_object("/secure/player"))) {
        write(sprintf("error loading player: %s\n", err));
        input_to("disconnectOnInput");
        return 0;
    }

    exec(playerObject, this_object());
    playerObject->initialise_player(_username, name);

    return playerObject;
}

void usernameEntered(string username) {
    if(sizeof(username)<1 || username == "") {
        write("You need to provide a username, so the game can identify you.\n");
        input_to("usernameEntered", INPUT_NOECHO|INPUT_PROMPT, "Your name: ");
        return;
    }

    _username = lower_case(username);
    
    int fsize = file_size(buildAccountSavefileName()+".o");
    if(fsize > 0 && restore_object(buildAccountSavefileName())) {        
        displayCharacterSelection();
    } else {
        input_to("createAccount", INPUT_PROMPT, "An account with this name does not exist. Create a new one? [Yes/no] ");
    }
}

int saveAccount() {
    int save = save_object(buildAccountSavefileName(), SAVEFILE_FORMAT_VERSION);
    if(save != 0) {
        write(sprintf("error: %d\n", save));
    }
    return save;
}

void createAccount(string answer) {
    answer = lower_case(answer);
    if(answer != "" && answer != "yes" && answer != "y") {
        write("Okay. Bye!\n");
        destruct(this_object());
        return;
    }

    if(file_size(sprintf("/characters/%s", _username)) != FSIZE_DIR) {
        if(!mkdir(sprintf("/characters/%s", _username))) {
            write("failed to create account: character storage could not be created.\n");
            input_to("disconnectOnInput");
        }
    }
    saveAccount();
    startCharacterGeneration();
}

void generateCharacter(string characterName) {
    object playerObject;

    if(characterName == "" || sizeof(characterName) == 0) {        
        input_to("generateCharacter", INPUT_PROMPT, "Please provide a name: ");
        return;
    }

    if(file_size(sprintf("players/%s.o", characterName)) > 0) {
        input_to("generateCharacter", INPUT_PROMPT, "A character with this name already exists. Please provide a different name: ");
        return;
    }

    playerObject = loadCharacter(characterName);

    _characters += ({ characterName });

    saveAccount();
    destruct(this_object());
}

void disconnectOnInput(string input) {
    destruct(this_object());
}