#include "/include/authority.h"
#include "/include/files.h"
#include "/include/commands.h"
#include "/include/ansi.h"
#include "/include/options.h"

inherit "/base/movable";

string _accountName;
string _characterName;
int _authorityLevel;
string _options;

varargs int save_me(mixed args);
private string get_save_file();
private void set_character_name(string name);
private void add_admin_commands();
private void add_default_actions();
private void move_to_last_location();
private void set_default_options();

public void create() {
    movable::create();
}

public void initialise_player(string accountName, string characterName) {
    set_character_name(characterName);
    _accountName = accountName;
    _authorityLevel = 0;
    set_default_options();

    if(!restore_object(get_save_file())) {
        write("new character!\n");
        save_me();
    } else {
        if(_accountName != accountName) {
            write("This character does not belong to your account!\n");
            destruct(this_object());
        }
        write("Welcome back!\n");
    }

    add_default_actions();
    add_admin_commands();

    move_to_last_location();
}

private void set_default_options() {
    _options = set_bit("", 
        OPT_P_AUTOLOOK)
    ;
}

private void add_default_actions() {
    add_action("quit", "quit");
    add_action("save_me", "save");
    add_action("whoami", "whoami");
    add_action("describe_something", "look");
    add_action("set_option", "set");
    add_action("check_external_commands", "", AA_SHORT|AA_IMM_ARGS);
}

public int set_option(string args) {
    notify_fail("set <option> <on|off>\n");
    if(args == 0 || args == "") {
        return 0;
    }

    string option, flag;
    if(sscanf(args, "%s %s", option, flag) != 2) {
        return 0;
    }

    if(flag != "on" && flag != "off") {
        return 0;
    }

    notify_fail("Undefined option.\n");
    int opt = 0;
    switch(option) {
        case "autolook": opt = OPT_P_AUTOLOOK; break;
        default: return 0;
    }

    if(flag == "on") {
        _options = set_bit(_options, opt);
    } else {
        _options = clear_bit(_options, opt);
    }

    write(sprintf("%s = %d\n", option, test_bit(_options, opt)));
    return 1;
}

public int check_external_commands(string input) {
    if(objectp(environment(this_player()))) {
        object area = environment(this_player());
        if(area->is_area()) {
            if(area->execute_area_command(input)) {
                return 1;
            }
        }
    }
    return 0;
}

private object find_detail_at_current_position(string key, object area) {
    string *zones = area->current_zones_of(this_object());
    if(sizeof(zones) == 0) {
        return 0;
    }

    foreach(string zone: zones) {
        object zobj = area->get_zone_object(zone);
        if(zobj->has_detail(key)) {
            return zobj;
        }
    }

    return 0;
}

private object find_source_of_detail(string id) {
    object src = 0;
    if(environment(this_object())->is_area()) {
        src = find_detail_at_current_position(id, environment(this_object()));
    }

    return src;
}

public int describe_something(string args) {
    if(args == "" || args == 0) {
        string description = environment(this_object())->get_description();
        write(terminal_colour(sprintf("%^NORMAL%^%s%^NORMAL%^", description), DEFAULT_ANSI_COLOURS));
    } else {
        object src = find_source_of_detail(args);
        if(!objectp(src)) {
            write(terminal_colour("%^RED%^You cannot find that.%^NORMAL%^\n", DEFAULT_ANSI_COLOURS));
            return -1;
        }
        
        string description = src->get_detail(args);
        int lpos = sizeof(description) - 1;
        if(description[lpos..lpos] != "\n") {
            description += "\n";
        }
        write(description);
        return 1;
    }
    return 1;
}

public int whoami(string args) {
    write(sprintf("You are %s.\n", _characterName));
    return 1;
}

public int quit(string args) {
    save_me();
    write("Bye!\n");
    destruct(this_object());  
    return 1;
}

varargs int save_me(mixed args) {
    write("Saved.\n");
    save_object(get_save_file());
    return 1;
}

private string get_save_file() {
    return sprintf("characters/%s/%s", lower_case(_accountName), lower_case(_characterName));
}

private void set_character_name(string name) {
    _characterName = capitalize(lower_case(name));
}

private void add_admin_commands() {
    if(_authorityLevel < AUTH_ADMIN) {
        return;
    }

    add_action("load_blueprint", "load");
    add_action("destruct_object", "destruct");
    add_action("move_to", "goto");
    add_action("print_my_environment", "myenv");
}

int print_my_environment(string args) {
    struct Coordinates pos = get_current_position();
    write(sprintf("You are here: %O <%s:%d:%d:%d>\n", environment(this_object()), pos->area, pos->x, pos->y, pos->z));
    return 1;
}

int move_to(string args) {
    notify_fail("syntax: goto <area> <x> <y>\n");
    if(args == "" || args == 0) {
        return 0;
    }

    string area;
    int x, y;
    int n = sscanf(args, "%s %d %d", area, x, y);
    if(n != 3) {
        return 0;
    }

    struct Coordinates np = (<Coordinates> area, x, y, 0);
    int success = move_to_coordinates(this_object(), np);
    write(sprintf("move to <%s:%d:%d:%d> = %d\n", np->area, np->x, np->y, np->z, success));
    return 1;
}

int load_blueprint(string path) {
    notify_fail("Which file should be loaded?\n");
    if(path == "" || path == 0) return 0;

    if(file_size(sprintf("%s.c", path)) < 0) {
        write("no such file.\n");
        return 1;
    }

    if(objectp(find_object(path))) {
        write("already loaded.\n");
        return -1;
    }

    object ob;
    string err;
    if(err = catch(load_object(path))) {
        write(sprintf("loading object '%s' failed: %s\n", path, err));
        return -1;
    }

    write("loaded.\n");
    return 1;
}

int destruct_object(string args) {
    notify_fail("no target specified.\n");
    if(args == "" || args == 0) return 0;

    object target;
    target = present(args, this_player());

    if(!objectp(target)) {
        target = find_object(args);
        if(!objectp(target)) {
            write(sprintf("could not find '%s'.\n", args));
            return -1;
        }
    }

    destruct(target);
    write(sprintf("destructed: %s\n", args));
    return 1;
}

private void move_to_last_location() {
    struct Coordinates lastPosition = get_current_position();

    object area =  find_object(lastPosition->area);
    if(!objectp(area)) {
        area = load_object(lastPosition->area);
    }

    move_to_coordinates(this_object(), lastPosition);
    if(environment(this_object()) != area) {
        write("Failed to move you back to where you left the game the last time.\n");
    }
}

void on_moved(struct Coordinates oldPosition, struct Coordinates newPosition) {
    if(test_bit(_options, OPT_P_AUTOLOOK)) {
        describe_something("");
    }
    return;
}

public int get_vision_range() {
    return 6;
}
