# Synopsis
Areas repersent parts of the game world where players can move and act consistently.

# API
## void initialize_map(int width, int height)
This function needs to be called before any other operations on the area. Usually it will
be called during the `create()` initialisation of the object. It sets up a plain and empty
map grid.

## add_barrier(int *from, int *to, string name, string block_string, string description)
Calling `add_barrier(...)` creates a barrier or wall bewtween the two given coordinates.
Characters will not be able to move between the coordinates and instead be prompted with 
an according description in case they try.