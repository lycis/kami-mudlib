# Events
All general event functions are named "on_*".

## General Movement
### void on_enter(object ob)
The given object has entered the inventory of this object.

### void on_neighbour_arrived(object ob)
The given object arrived within the same environment.

### void on_exit(object ob)
The given object left the inventory of this object.

## Area Specific
### on_moved_to(object ob, struct Coordinates position)
Object `ob` moved onto the given `position` within the area. During movement between areas
this function will be called for the new area.

### on_moved_from(object ob, struct Coordinats position)
`ob` moved away from the given `positon` within the area. During movement between areas
this function will be called for the previous area.

### on_moved(struct Coordinates oldPosition, struct Coordinates newPosition)
called within an object after it was moved to a new position within the game world.