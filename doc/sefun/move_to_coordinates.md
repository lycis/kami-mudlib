# Synopsis
int move_to_coordinates(object ob, struct Coordinates newPosition)

# Description
This functions moves an object to the given coordinates within the world.

# See Also
