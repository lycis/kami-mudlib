inherit "/base/zone";

public void create() {
    ::create();

    add_zone_action("dance", ({"dance"}));
    set_description(
        "You are at the town square. The ground is graveled and "
        "well-trodden from years of usage. This open space is also used "
        "for the weekly market and other special occasions.");
    set_remote_description("the town square of Honeywood");

    add_detail("ground", "The groun of the town square is graveled with small but spikey stones. "
               "Here and there you see patches of mud where the ground is not fully covered.");

    add_detail(({"stone", "stones"}), "Small gravel that hurts pretty badly when you cross it without shoes.");
}

public int dance(string args) {
    notify_fail("What do you want to dance?\n");
    if(args != "jig") return 0;

    write("You dance a jig.\n");
    return 1;
}