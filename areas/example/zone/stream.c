inherit "/base/zone";

public void create() {
    ::create();

    set_description(
        "You are wading through a small stream that crosses the village. "
        "It is just high enough to reach your ankles and get your shoes wet. "
        "The water makes it way from east to west with faint gurgling noises.");
    set_remote_description("a small stream");
}
