<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="borders" tilewidth="32" tileheight="32" tilecount="16" columns="4">
 <image source="borders.png" width="128" height="128"/>
 <tile id="0" type="border">
  <properties>
   <property name="direction" type="int" value="64"/>
  </properties>
 </tile>
 <tile id="1" type="border">
  <properties>
   <property name="direction" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="2" type="border">
  <properties>
   <property name="direction" type="int" value="4"/>
  </properties>
 </tile>
</tileset>
