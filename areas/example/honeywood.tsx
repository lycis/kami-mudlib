<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="honeywood" tilewidth="32" tileheight="32" tilecount="483" columns="21">
 <image source="terrain.png" width="672" height="736"/>
 <tile id="64" type="zone">
  <properties>
   <property name="zoneFile" value="/areas/example/zone/town_square"/>
   <property name="zoneId" value="town_square"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="zoneFile" value="/areas/example/zone/stream"/>
   <property name="zoneId" value="stream"/>
  </properties>
 </tile>
 <tile id="477" type="barrier">
  <properties>
   <property name="direction" value="north"/>
  </properties>
 </tile>
</tileset>
