inherit "/base/area";

void create() {
    ::create();
    ::initialize_map(50, 50);

    set_description(
        "This is the small town of Honeywood. It is a quiet village in betwen the "
        "woods to the west and the mountains to the east.");

    add_detail(({"town", "honeywood", "village"}),
        "This town is called Honeywood.");

    add_detail( ({"wood", "woods", "tree", "trees"}),
        "The dark woods are located west of the town. They are known to house vile bandits, "
        "old ruins, but are a source of living for the inhabitants of Honeywood." );
    
    add_detail( ({"mountains", "mountain", "territory"}), 
        "The mountains of the east are dangerous territory. Apart from strange orcs and ogres, "
        "the only folks living there are dwarves.");

    add_detail( ({"bandits", "travellers", "caravans"}),
        "Bandits are known to house in the woods. Sometimes they attack travellers and caravans." );

    add_detail( ({"ruins"}), "You can find them within the woods." ); 

    add_detail( ({"source", "living", "income"}), 
        "The trees from the woods serve as main source of income for this small town." );

    add_detail( ({"inhabitants", "people", "folks"}), 
        "Honeywood is inhabited by a mix of friendly people. Although they are villagers, they "
        "are known to be friendly to travelling adventurers, who relieve them of their problems." );

    add_detail( ({"orcs", "ogres"}), 
        "The orcs and ogres living in the mountains are dangerous and wild. Sometimes they sweep "
        "down from the east to raid the town." );

    add_detail( ({"dwarves"}), 
        "The stout dwarves are well-known here in Honeywoody. Regularily, on market day, some of them "
        "open up their stands on the town square." );

    add_detail( ({"problem", "problems", "support"}), "Look around. There may be some people, who need your support." );

    add_zone(({ ({23, 23, 0}), ({24, 23, 0}), ({25, 23, 0}),
                ({23, 24, 0}), ({24, 24, 0}), ({25, 24, 0}),
                ({23, 25, 0}), ({24, 25, 0}), ({25, 25, 0})}) ,
             "town_square", "/areas/example/zone/town_square");
    add_zone( ({ ({0, 38, 0}), ({1, 38, 0}), ({2, 37, 0}), ({3, 36, 0}),
                 ({4, 36, 0}), ({6, 36, 0}), ({7, 36, 0}), ({8, 36, 0}),
                 ({8, 36, 0}), ({10, 36, 0}), ({11, 37, 0}), ({12, 39, 0}),
                 ({12, 39, 0}), ({14, 39, 0}), ({15, 39, 0}), ({15, 37, 0}),
                 ({14, 38, 0}), ({16, 36, 0}), ({17, 36, 0}), ({18, 35, 0}),
                 ({18, 35, 0}), ({20, 34, 0}), ({21, 33, 0}), ({22, 33, 0}),
                 ({22, 33, 0}), ({23, 32, 0}), ({24, 32, 0}), ({25, 32, 0}),
                 ({25, 30, 0}), ({26, 30, 0}), ({27, 30, 0}), ({28, 30, 0}),
                 ({28, 29, 0}), ({30, 29, 0}), ({31, 29, 0}), ({33, 29, 0}),
                 ({33, 29, 0}), ({35, 28, 0}), ({36, 27, 0}), ({37, 26, 0}),
                 ({37, 26, 0}), ({39, 26, 0}), ({40, 25, 0}), ({41, 25, 0}),
                 ({40, 24, 0}), ({42, 24, 0}), ({42, 23, 0}), ({43, 23, 0}),
                 ({42, 22, 0}), ({44, 22, 0}), ({45, 22, 0}), ({45, 21, 0}),
                 ({45, 20, 0}), ({47, 19, 0}), ({48, 19, 0}), ({49, 19, 0}),
                 ({49, 19, 0}) }),
            "stream", "/areas/example/zone/stream");

    add_barrier(({15, 36, 0}), ({16, 36, 0}), "bushes", "are blocking your way", "A batch of bushes is blocking your way to the %s.");
}
