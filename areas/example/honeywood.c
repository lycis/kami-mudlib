inherit "/base/area";

public void create() {
    ::create();
    load_from_tmx("/areas/example/honeywood.tmx");

    set_description(
        "This is the small town of Honeywood. It is a quiet village in betwen the "
        "woods to the west and the mountains to the east.");

    add_detail(({"town", "honeywood", "village"}),
        "This town is called Honeywood.");

    add_detail( ({"wood", "woods", "tree", "trees"}),
        "The dark woods are located west of the town. They are known to house vile bandits, "
        "old ruins, but are a source of living for the inhabitants of Honeywood." );
    
    add_detail( ({"mountains", "mountain", "territory"}), 
        "The mountains of the east are dangerous territory. Apart from strange orcs and ogres, "
        "the only folks living there are dwarves.");

    add_detail( ({"bandits", "travellers", "caravans"}),
        "Bandits are known to house in the woods. Sometimes they attack travellers and caravans." );

    add_detail( ({"ruins"}), "You can find them within the woods." ); 

    add_detail( ({"source", "living", "income"}), 
        "The trees from the woods serve as main source of income for this small town." );

    add_detail( ({"inhabitants", "people", "folks"}), 
        "Honeywood is inhabited by a mix of friendly people. Although they are villagers, they "
        "are known to be friendly to travelling adventurers, who relieve them of their problems." );

    add_detail( ({"orcs", "ogres"}), 
        "The orcs and ogres living in the mountains are dangerous and wild. Sometimes they sweep "
        "down from the east to raid the town." );

    add_detail( ({"dwarves"}), 
        "The stout dwarves are well-known here in Honeywoody. Regularily, on market day, some of them "
        "open up their stands on the town square." );

    add_detail( ({"problem", "problems", "support"}), "Look around. There may be some people, who need your support." );

}