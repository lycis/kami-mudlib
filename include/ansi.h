#ifndef __ANSI__H_
#define __ANSI__H_

#define ANSI_BOLD "\e[1m"
#define ANSI_UNDERL "\e[4m"
#define ANSI_BLINK "\e[5m"
#define ANSI_INVERS "\e[7m"

#define ANSI_BLACK "\e[30m"
#define ANSI_RED "\e[31m"
#define ANSI_GREEN "\e[32m"
#define ANSI_YELLOW "\e[33m"
#define ANSI_BLUE "\e[34m"
#define ANSI_PURPLE "\e[35m"
#define ANSI_CYAN "\e[36m"
#define ANSI_WHITE "\e[37m"

#define ANSI_BG_BLACK "\e[40m"
#define ANSI_BG_RED "\e[41m"
#define ANSI_BG_GREEN "\e[42m"
#define ANSI_BG_YELLOW "\e[43m"
#define ANSI_BG_BLUE "\e[44m"
#define ANSI_BG_PURPLE "\e[45m"
#define ANSI_BG_CYAN "\e[46m"
#define ANSI_BG_WHITE "\e[47m"

#define ANSI_CLEAR "\e[H\e[2J"
#define ANSI_NORMAL "\e[0m"

#define DEFAULT_ANSI_COLOURS ([ \
    "BOLD":      "\e[1m", \
    "UNDERLINE": "\e[4m", \
    "BLINK":     "\e[5m", \
    "INVERS":    "\e[7m", \
    "BLACK":     "\e[30m", \
    "RED":       "\e[31m", \
    "GREEN":     "\e[32m", \
    "YELLOW":    "\e[33m", \
    "BLUE":      "\e[34m", \
    "PURPLE":    "\e[35m", \
    "CYAN":      "\e[36m", \
    "WHITE":     "\e[37m", \
    "BBLACK":   "\e[40m", \
    "BRED":     "\e[41m", \
    "BGREEN":   "\e[42m", \
    "BYELLOW":  "\e[43m",\
    "BBLUE":    "\e[44m", \
    "BPURPLE":  "\e[45m", \
    "BCYAN":    "\e[46m", \
    "BWHITE":   "\e[47m", \
    "CLEAR":     "\e[H\e[2J", \
    "NORMAL":    "\e[0m"])

#define ansi_coloured(x) terminal_colour(x, DEFAULT_ANSI_COLOURS)

#endif
